require('lualine').setup {
  options = {
    theme = 'tokyonight'
  }
}

require('lspconfig').clangd.setup {
  on_attach = require('completion').on_attach,
  cmd = { "xcrun", "clangd", "--background-index" },
  filetypes = { "c", "cpp", "objc", "objcpp" }
}

-- Disable displaying compile errors automatically
vim.lsp.handlers["textDocument/publishDiagnostics"] = function() end

require('telescope').setup {
  defaults = { file_ignore_patterns = { "./build", "%.bin" } }
}
