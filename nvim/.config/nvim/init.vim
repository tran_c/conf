set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath

source ~/.vim/misc.vim
source ~/.vim/display.vim
source ~/.vim/mapping.vim

let g:polyglot_disabled = ['sensible']

call plug#begin(stdpath('data') . '/plugged')

Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'folke/tokyonight.nvim'
Plug 'tpope/vim-commentary'
Plug 'yegappan/taglist'
Plug 'sheerun/vim-polyglot'
Plug 'Townk/vim-autoclose'
Plug 'tpope/vim-fugitive'

Plug 'hoob3rt/lualine.nvim'
Plug 'ryanoasis/vim-devicons' " required by lualine for icons

" telescope + dependencies
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

Plug 'neovim/nvim-lspconfig'
Plug 'nvim-lua/completion-nvim'
Plug 'kristijanhusak/completion-tags'

call plug#end()

let g:tokyonight_style = "storm"
let g:tokyonight_italic_functions = 1
let g:tokyonight_sidebars = [ "qf", "vista_kind", "terminal", "packer" ]
colorscheme tokyonight

set completeopt=menuone,noinsert,noselect
set shortmess+=c
let g:completion_enable_auto_popup = 0
imap <C-Space> <Plug>(completion_trigger)
" let g:completion_chain_complete_list = {
"       \ 'default': [
"       \    {'complete_items': ['lsp', 'tags']},
"       \  ]}
let g:completion_matching_strategy_list = ['exact', 'substring', 'fuzzy', 'all']
let g:completion_matching_smart_case = 1

" Highlight trailing whitespaces
highlight ExtraWhitespace ctermbg=red guibg=lightred
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/

" Highlight non-breakable spaces
highlight NbSp ctermbg=lightred guibg=lightred
2match NbSp /\%xA0/
autocmd BufWinEnter * 2match NbSp /\%xA0/

autocmd FileType cpp,c setlocal commentstring=//\ %s

lua require('init-lua')
