# Set up the prompt

precmd() {

if [ $UID -eq 0 ]; then
    cuser="red"
    prompt="#"
else
    cuser="cyan"
    prompt=""
fi;

chost="green"
cpath="red"
ctime="yellow"

time="%H:%M"
timestamp="%(?..%B%{$fg[$cpath]%}Err %?%b$clr )%{$fg[$cpath]%}|$clr%{$fg[$ctime]%}%B%D{$time}"

user="%{$fg[$cuser]%}%n"
host="%{$fg[$chost]%}%m"
pwd="%{$fg[$cpath]%}%~"

clr="%{$reset_color%}"

PS1="$user$clr@$host$clr:$pwd$clr$prompt> "
RPS1="$timestamp$clr"

}

# Enable advanced prompt
#autoload -Uz promptinit
#promptinit
#prompt adam1

if [ -r ~/.zshrc-aux ]; then
    # local zsh options - set your PATH variable in this file
    source ~/.zshrc-aux
fi

#setopt correctall
setopt histignorealldups sharehistory

# Use emacs keybindings even if our EDITOR is set to vi
bindkey -e

# Keep 1000 lines of history within the shell and save it to ~/.zsh_history:
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.histfile

# Use modern completion system
autoload -Uz compinit
compinit

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
# sudo port install coreutils +with_default_names to get dircolors under OS X
if [ `uname` = 'Darwin' ]; then
    eval "$(gdircolors -b)"
else
    eval "$(dircolors -b)"
fi
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

if [ -r ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]; then
    source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
    ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern)
    ZSH_HIGHLIGHT_STYLES[path]='fg=cyan,underline'
    ZSH_HIGHLIGHT_PATTERNS+=('rm -rf *' 'fg=white,bold,bg=red')
fi

autoload -U colors
colors
LS_COLORS='no=00:fi=00:di=00;36:ln=00;32:pi=00;33:so=00;33:bd=00;33:cd=00;33:ex=00;35:';
CLICOLOR=cons25

export LS_COLORS
export CLICOLOR
if command -v most > /dev/null; then
    export PAGER='most'
else
    export PAGER='less'
fi
export HGENCODING='utf8'

if [ `uname` = "Darwin" ]; then
    export EDITOR='mvim -v'

    alias ls='ls -G'
    alias grep='grep --color -n'
else
    export EDITOR='vim'

    alias ls="ls --color"
    alias grep='grep --color -n --exclude-dir={.svn,.hg,.git,log}'
fi
