#! /bin/bash

# Get absolute path of the conf directory
pwd=`dirname $0`
pwd=`cd $pwd; pwd`
args=( $* )

zsh_path="zsh"
vim_path="vim"
irssi_path="."
x_path="."
hg_path="."
git_path="."
tmux_path="."

zsh="$zsh_path/.zsh $zsh_path/.zshrc"
vim="$vim_path/.vimrc $vim_path/.vim"
irssi="$irssi_path/.irssi"
xdefaults="$x_path/.Xdefaults"
mercurial="$hg_path/.hgrc"
git="$git_path/.gitconfig"
tmux="$tmux_path/.tmux.conf"

function create_link()
{
    for file in $@; do
	echo -e "\033[32mCreated symbolic link : \033[0m$HOME/$file -> $pwd/$file"
	ln -fs "$pwd/$file" "$HOME/"
    done
}

if [ $# == 0 ]; then
    echo "Usage : display.sh [zsh vim irssi xdefaults hg git server mac all]"
    echo "server : install everything except useless things for servers"
    echo "mac : install everything except useless things for Mac OS X"
    echo "all : install everything"
else
    for arg in ${args[@]}; do
	case $arg in
	    zsh)
		create_link $zsh
		;;
	    vim)
		create_link $vim
		;;
	    irssi)
		create_link $irssi
		;;
	    xdefaults)
		create_link $xdefaults
		;;
	    hg)
		create_link $mercurial
		;;
	    git)
		create_link $git
		;;
	    tmux)
		create_link $tmux
		;;
	    server)
		create_link $zsh
		create_link $vim
		create_link $irssi
		create_link $mercurial
		create_link $git
		create_link $tmux
		;;
	    mac)
		create_link $zsh
		create_link $vim
		create_link $irssi
		create_link $mercurial
		create_link $git
		create_link $tmux
		;;
	    all)
		create_link $zsh
		create_link $vim
		create_link $irssi
		create_link $xdefaults
		create_link $mercurial
		create_link $git
		create_link $tmux
		;;
	    *)
		echo -e "\033[31mUnknown argument \"$arg\""
		echo -e "\033[31mCall install.sh without arguments to display help"
		;;
	esac
    done
fi
