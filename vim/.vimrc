source ~/.vim/misc.vim
source ~/.vim/display.vim
source ~/.vim/mapping.vim

call plug#begin("~/.vim/plugged")

Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'tpope/vim-commentary'
Plug 'bling/vim-airline'
Plug 'ghifarit53/tokyonight-vim'
Plug 'tpope/vim-endwise'
Plug 'tpope/vim-repeat'
Plug 'sheerun/vim-polyglot'

Plug 'hoob3rt/lualine.nvim'
Plug 'ryanoasis/vim-devicons' " required by lualine for icons

Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets' " snippets for ultisnips

call plug#end()

" This needs to be run after loading the tokyonight plugin
if has("gui_running")
    let g:airline_theme = "tokyonight"
    set termguicolors
    let g:tokyonight_style = 'storm'
    let g:tokyonight_enable_italic = 1
    colorscheme tokyonight
endif

if filereadable(expand('~/.vim/vimrc-local.vim'))
    source ~/.vim/vimrc-local.vim
endif
