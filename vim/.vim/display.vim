set showmatch
set number
set relativenumber
set ruler
set cursorline
set showcmd
set hlsearch
set guioptions-=m
set guioptions-=r
set guioptions-=L
" Enable 256 colors
set t_Co=256

" For GVim and MacVim
if has("gui_running")
    set background=dark
    colorscheme moria
else
    colorscheme candycode
endif

" Highlight trailing whitespaces
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/

" Highlight non-breakable spaces
highlight NbSp ctermbg=lightred guibg=lightred
2match NbSp /\%xA0/
autocmd BufWinEnter * 2match NbSp /\%xA0/

set colorcolumn=80
highlight ColorColumn ctermbg=236 guibg=black

autocmd BufWinLeave * call clearmatches()

function s:LoadSyntax ()
    source ~/.vim/syntax/opengl.vim
    source ~/.vim/syntax/libsdl.vim
endfunction
autocmd FileType c,cpp call s:LoadSyntax()
