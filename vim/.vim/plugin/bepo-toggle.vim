" Quick Bepo-layout toggle.
" when toggled, you can use ctsr to move
" when untoggled, ctsr will behave as expected by vim
"
" This code is under BSD licence, and the author doesn't even bother tell his identity.

let s:BepoToggleState = 0
let s:BepoToggleActivated = s:BepoToggleState

function! g:BepoToggle ()
    if s:BepoToggleState
	set langmap=
	let s:BepoToggleState = 0
    else
	set langmap=ctsrCTSRhjklHJKL;hjklHJKLctsrCTSR
	let s:BepoToggleState = 1
    endif
endfunction

function s:SetCorrectLangmapEnter ()
    let s:BepoToggleActivated = s:BepoToggleState
    if s:BepoToggleActivated
	call g:BepoToggle()
    endif
endfunction

function s:SetCorrectLangmapLeave ()
    if s:BepoToggleActivated
	call g:BepoToggle()
    endif
endfunction

" use à or whatever you want. Make it easy to access though.
nmap à :call g:BepoToggle()<CR>

" When we enter insert mode we want to come back to the default langmap
autocmd InsertEnter * call s:SetCorrectLangmapEnter()
autocmd InsertLeave * call s:SetCorrectLangmapLeave()
