imap <C-n> <C-j>
imap <C-k> <UP>
imap <C-j> <DOWN>
imap <C-h> <LEFT>
imap <C-l> <RIGHT>
nmap :W :w
nmap :Q :q
nmap :Noh :noh
nmap <S-Tab> :FufFile<CR>
nmap <C-b> :NERDTree
map <C-PageUp> <ESC>:tabprevious<CR>
map <C-PageDown> <ESC>:tabnext<CR>
" build tags of your own project with Ctrl-F12
map <C-F12> :!ctags -R --sort=yes --c++-kinds=+p --fields=+iaS --extra=+q .<CR>

" UltiSnips keys
let g:UltiSnipsExpandTrigger="<Tab>"
let g:UltiSnipsJumpForwardTrigger="<C-b>"
let g:UltiSnipsJumpBackwardTrigger="<C-z>"

" Vim surround plugin mappings to avoid conflicts with a modified langmap
nmap dk  <Plug>Dkullound
nmap ck  <Plug>Hkullound
nmap yk  <Plug>Ykullound
nmap yk  <Plug>Ykullound
nmap ykk <Plug>Ykkullound
nmap ykk <Plug>Ykkullound
nmap ykk <Plug>Ykkullound
xmap s   <Plug>Vkullound
xmap gk  <Plug>Vgkullound

" Vim commentary plugin mappings to avoid conflicts with a modified langmap
xmap gh  <Plug>Hommenjaly
nmap gh  <Plug>Hommenjaly
omap gh  <Plug>Hommenjaly
nmap ghh <Plug>HommenjalyRine
nmap cgc <Plug>HcangeHommenjaly
nmap ghu <Plug>Hommenjaly<Plug>Hommenjaly

if has('nvim')
    tnoremap <Esc> <C-\><C-n>
    nnoremap <S-Tab> <cmd>Telescope find_files<cr>
endif
