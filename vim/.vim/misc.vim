" Share clipboard with system clipboard
set clipboard=unnamed
set nocompatible
set bs=2
" set textwidth=79
" set wrapmargin=8
syntax on

set fileencoding=utf8
set encoding=utf8

" ignore case except if uppercase characters are used
set ignorecase
set smartcase

set wildmode=list:longest
set wildignore=*.o
set wildmenu

set mouse=a

" Set to auto read when a file is changed from the outside
set autoread

" No sound on errors
set noerrorbells
set novisualbell

" Tab
set tabstop=8
set shiftwidth=4
set softtabstop=8
" Replace tab with spaces
set expandtab
set noautoindent
set smarttab
set nocp
filetype plugin indent on

" Allow tabs in a Makefile
autocmd FileType make set noexpandtab shiftwidth=8 softtabstop=8

set cindent
set cinoptions=(0

set backup
if has("unix")
    set backupdir=~/.vimtmp/backup
    set directory=~/.vimtmp/swap

    silent !mkdir -p ~/.vimtmp/backup
    silent !mkdir -p ~/.vimtmp/swap
endif

" OmniCppComplete
let OmniCpp_NamespaceSearch = 1
let OmniCpp_GlobalScopeSearch = 1
let OmniCpp_ShowAccess = 1
let OmniCpp_ShowPrototypeInAbbr = 1 " show function parameters
let OmniCpp_MayCompleteDot = 0 " autocomplete after .
let OmniCpp_MayCompleteArrow = 0 " autocomplete after ->
let OmniCpp_MayCompleteScope = 0 " autocomplete after ::
let OmniCpp_DefaultNamespaces = ["std", "_GLIBCXX_STD"]
" automatically open and close the popup menu / preview window
au CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif
set completeopt=menuone,menu,longest,preview

" Ruby complete
autocmd FileType ruby,eruby set omnifunc=rubycomplete#Complete
autocmd FileType ruby,eruby let g:rubycomplete_buffer_loading = 1
autocmd FileType ruby,eruby let g:rubycomplete_rails = 1
autocmd FileType ruby,eruby let g:rubycomplete_classes_in_global = 1

" For GVim and MacVim
if has("gui_running")
    set guifont=DejaVu\ Sans\ Mono\ 12
    set guioptions-=T
    " Disable bell
    set vb t_vb=
endif
